$(function() {

	$("#menu").click(function() {
		$("#toggle").slideToggle("active");
	});

	var accordions = $(".accordion-item");

	accordions.each(function() {
		$(this).click(function(e) {
			e.stopPropagation();
			if ($(this).hasClass("open")) {
				$(this).removeClass("open")
				return
			} else {
				var ans = $(this).find(".answer")
				$(".accordion-item.open").removeClass("open");
				$(this).toggleClass("open")
			}
		})
	});

});
