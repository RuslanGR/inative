from django.urls import path
from django.views.generic import (
    TemplateView
)

from .views import (
    CategoryView,
    ProductView,
    UslugiListView,
)

urlpatterns = (
    path('', TemplateView.as_view(template_name='central/index.html'), name='index'),
    path('services/', UslugiListView.as_view(), name='services'),
    path('shop/', TemplateView.as_view(template_name='central/shop.html'), name='shop'),
    path('category/<slug:category>/', CategoryView.as_view(), name='category'),
    path('product/<slug:slug>', ProductView.as_view(), name='product'),
)
