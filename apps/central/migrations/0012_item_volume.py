# Generated by Django 3.0.2 on 2020-01-13 22:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('central', '0011_auto_20200114_0108'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='volume',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Обьем (мл)'),
        ),
    ]
