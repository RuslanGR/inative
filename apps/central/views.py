from django.shortcuts import render, get_object_or_404
from django.views import View
from django.views.generic import ListView

from .models import (
    Category,
    Item,
    Service,
)


class CategoryView(View):

    def get(self, request, category):
        cat = get_object_or_404(Category, slug=category)

        if cat.subcategory:
            cat = cat.subcategory
        products = Item.objects.filter(category=cat)

        return render(request, 'central/category.html', {'products': products})


class ProductView(View):

    def get(self, request, slug):
        product = get_object_or_404(Item, slug=slug)
        return render(request, 'central/product.html', {'product': product})


class UslugiListView(ListView):

    model = Service
    context_object_name = 'services'
    template_name = 'central/services.html'
