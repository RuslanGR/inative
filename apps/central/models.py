from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify

from .utils.text import transliterate


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Item(models.Model):
    name = models.CharField(max_length=120)
    price = models.DecimalField(decimal_places=2, max_digits=8)
    description = models.TextField(blank=True, null=True)
    category = models.ForeignKey('Category', on_delete=models.CASCADE)
    img = models.ImageField(upload_to='product_images', blank=True, null=True)
    volume = models.PositiveIntegerField(verbose_name='Обьем (мл)', blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        translated_text = transliterate(self.name)
        self.slug = slugify(translated_text)

        super().save(*args, **kwargs)


class OrderItem(models.Model):
    item = models.ForeignKey('Item', on_delete=models.CASCADE)
    order = models.ForeignKey('Order', on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)


class Category(models.Model):
    """Category model"""

    name = models.CharField(max_length=120)
    subcategory = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    slug = models.SlugField(blank=True, null=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def save(self, *args, **kwargs):
        translated_text = transliterate(self.name)
        self.slug = slugify(translated_text)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Service(models.Model):
    """Uslugi model"""

    title = models.CharField(max_length=240)
    description = models.TextField()
    price = models.DecimalField(max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

    def __str__(self):
        return self.title
